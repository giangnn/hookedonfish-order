var app = angular.module('hof', ['pouchdb']);

app.controller('MainCtrl', function ($log, $scope, pouchDB) {
    

    function error(err) {
        $log.error(err);
    }

    function get(res) {
        if (!res.ok) {
            return error(res);
        }
        return db.get(res.id);
    }

    function bind(res) {
        $scope.doc = res;
    }

    function order(itemId) {
        db.post({
            state: 0,
            menuId: itemId,
        });
    }   

    function processChangedItem(item) {
        var state = item.state;
        // handle change
        if (state === 0) {
            $scope.pendingItems[item.menuId]++;            
        } else if (state === 1) {
            $scope.pendingItems[item.menuId]--;
            $scope.processingItems[item.menuId]++;
        } else if (state === 2) {
            $scope.processingItems[item.menuId]--;
            $scope.finishedItems[item.menuId]++;
        } else if (state === 3) {
            $scope.finishedItems[item.menuId]--;
            $scope.pickedItems[item.menuId]++;
        }
    }

    function onChange(change) {
        console.log('change');
        var item = change.change.doc;
        processChangedItem(item);
    } 

    function onSyncChange(info) {
        console.log(info);
        if (!info.change) {
            return;
        }
        console.log('process sync');
        var items = info.change.change.docs;
        _.each(items, processChangedItem);
        console.log(info);
        // var item = change.change.doc;
        // var state = item.state;
        // // handle change
        // if (state === 0) {
        //     $scope.pendingItems[item.menuId]++;            
        // } else if (state === 1) {
        //     $scope.pendingItems[item.menuId]--;
        //     $scope.processingItems[item.menuId]++;
        // } else if (state === 2) {
        //     $scope.processingItems[item.menuId]--;
        //     $scope.finishedItems[item.menuId]++;
        // } else if (state === 3) {
        //     $scope.finishedItems[item.menuId]--;
        //     $scope.pickedItems[item.menuId]++;
        // }
    } 
    
    var items = [
        {
            name: 'Salmon',
            main: false
        },
        {
            name: 'Basa',
            main: true
        }
    ];

    $scope.pendingItems = [0, 0];
    $scope.processingItems = [0, 0];
    $scope.finishedItems = [0, 0];
    $scope.pickedItems = [0, 0];

    $scope.items = items;
    $scope.order = order;

    // var changes = db.changes({
    //     since: 'now',
    //     live: true,
    //     include_docs: true
    // }).on('change', function (change) {
    //     var item = change.doc;
    //     var state = item.state;
    //     // handle change
    //     if (state === 0) {
    //         $scope.pendingItems[item.menuId]++;            
    //     } else if (state === 1) {
    //         $scope.pendingItems[item.menuId]--;
    //         $scope.processingItems[item.menuId]++;
    //     } else if (state === 2) {
    //         $scope.processingItems[item.menuId]--;
    //         $scope.finishedItems[item.menuId]++;
    //     } else if (state === 3) {
    //         $scope.finishedItems[item.menuId]--;
    //         $scope.pickedItems[item.menuId]++;
    //     }
    // }).on('complete', function (info) {
    //     // changes() was canceled
    // }).on('error', function (err) {
    //     console.log(err);
    // });

    var db = pouchDB('hof');

    db.sync('http://localhost:5984/hof1', {
                live: true,
                retry: true
            });

    db.changes({
        since: 'now',
        live: true,
        include_docs: true
    }).$promise.then(null, null, onChange);
});
